# -*- coding: utf-8 -*-

# from ws4py.client.threadedclient import WebSocketClient
from socketIO_client import SocketIO, BaseNamespace
import RPi.GPIO as GPIO
import time
import datetime
from collections import deque


# Setup the pins (we picked two BCM pins that have ground adjacent to them)
keyPin = 07
spkrPin = 10



# host = "127.0.0.1"
# port = 8000

# Set whether we're using a buzzer with built in oscillator (BUZZ) or speaker (PWM)
# toneOutputMode = "PWM" 
toneOutputMode = "BUZZ"

host = 'morse-autodidacts.rhcloud.com'
port = 8000

MSG_START_TRANSMISSION = 3
MSG_KEY_UP = 1
MSG_KEY_DOWN = 2
BUFFER_DELAY = 500000 # 500k = 1/2 s
TIMEOUT_INTERVAL = (10**6) * 3 

# We're using BCM
GPIO.setmode(GPIO.BCM)

# Set a pullup on the keyPin, and set it as input
GPIO.setup(keyPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# Speaker is output
GPIO.setup(spkrPin, GPIO.OUT)

# If we're using PWM output, set parameters
if(toneOutputMode == "PWM"):
  duty_cycle = 50
  frequency = 500  
  spkrPWM = GPIO.PWM(spkrPin, frequency)


# Get the current time in mocroseconds
def microseconds ():
  keytime = datetime.datetime.now()
  uS = keytime.microsecond  
  d = datetime.datetime.now()
  s = time.mktime(d.timetuple())
  return (s*1000000)+uS
  
  
  
def toggleSpeaker():
  if(speakerState == 0):
    speakerState = 1
  else:
    speakerState = 0

  setSpeaker(speakerState)

  
def safeInt(val):
    try:
      out = int(val)
    except ValueError:
      out = 0  
      
    return out
    
def dbg(in1,in2 = False):
  print(str(in1) +": "+ str(in2))
  
    
def setSpeaker(value):
  if(toneOutputMode == "PWM"):
    if(value == 1):
      spkrPWM.start(duty_cycle)
    else:
      spkrPWM.stop() 
  else:
    GPIO.output(spkrPin, value) 

class MorseClient():
  def opened(self):                
    # self.send("Hello, World!")
    print("Didn't send hello")

  def closed(self, code, reason):
    print(("Closed down", code, reason))

  def on_message(self, *args):
    # SocketIO sends message in ugly format. This is a hack.
    for arg in args:
        m = arg[12:len(arg)-3]    

    # Split the string into value + timestamp

    action = safeInt(str(m)[0:1])
        
    timestamp = safeInt(str(m)[1:])

    print(("Received message",action))

    if(action == MSG_START_TRANSMISSION):
     '''
      self.scOffset = microseconds() - timestamp # Offset between local time and server time (will also approximately include latency...)

      self.inputBuffer = deque() # initialize the buffer
      
      self.mode = "receive"
      
      print(("scOffset",self.scOffset))
      print("Transmission start")
      print(("buffer",self.inputBuffer))
        
      
      # could set the 'now receiving' LED to on here...      
    '''
    
    elif(action == MSG_KEY_DOWN):
      #self.inputBuffer.append((action,(timestamp + self.scOffset + BUFFER_DELAY)))
      #print(("buffer",self.inputBuffer))
      setSpeaker(1)
      
    elif(action == MSG_KEY_UP):
      #self.inputBuffer.append((action,(timestamp + self.scOffset + BUFFER_DELAY)))
      #print(("buffer",self.inputBuffer))
      setSpeaker(0)      
    
    else:
      print(("Unrecognized message",m))
    
    # Print interesting things for debugging
    print("message: "+str(m))
    print(("action: ",action))
    
    '''
    print("time elapsed: "+str(microseconds() - receiveTs))
    print("duration: "+dur)
 
    # The part that doesn't work:
    while (microseconds() - receiveTs) < dur:
      print("start audio")
      if value == 1 or value == 0:
        print("VV")
        GPIO.output(spkrPin, int(value))
      time.sleep(0.5)
      break
    print("stop audio")
    if value == 1 or value == 0:
      print("VV")
      GPIO.output(spkrPin, int(anti_value))
    '''


class MorseKey():
    
  def keyEvent(self, channel):
      
    input = GPIO.input(keyPin)

    '''    
      if (input == 0):
        print(("Sending msg",input))
        ws.emit(str(MSG_KEY_DOWN) + str(microseconds()))
      else:
        ws.emit(str(MSG_KEY_UP) + str(microseconds()))    

    '''
    
    if ((input == 0) and (key.last_state != 0)):
      print(("Sending msg",MSG_KEY_DOWN))
      ws.emit("morse",(str(MSG_KEY_DOWN) + str(microseconds())))
      setSpeaker(1)
      key.last_state = 0            
        
    elif input and (key.last_state != 1):
      print(("Sending msg",MSG_KEY_UP))
      ws.emit("morse",(str(MSG_KEY_UP) + str(microseconds())))
      setSpeaker(0)
      key.last_state = 1


    
  def listen(self, keyPin):
        GPIO.add_event_detect(keyPin, GPIO.BOTH, callback=self.keyEvent)

if __name__ == '__main__':
  
  speakerState = 0  

  
  try:
    ws = SocketIO(host, port)
    client = MorseClient()
    ws.emit('morse', 'Client listening...')
    ws.on('message', client.on_message)

    key = MorseKey()
    key.last_state = 1
    key.listen(keyPin)
    
    ws.wait()
    
    # ws = MorseClient('ws://'+host+':'+str(port)+'/ws', protocols=['http-only', 'morse'])
    # ws.daemon = False
    # ws.connect()
    #ws.send("Initialized")
  except KeyboardInterrupt:
    ws.close()
  
  '''
  client.inputBuffer = deque()

  key.last_state = 1
  key.last_dur = 0
  key.last_start = 0
  key.last_end = 0

  
  speakerState = 0
  
  nextTransition = False
  lastTransitionTime = microseconds()
  
  print("Entering the loop")
  
  while True:
    dbg("looping")
    dbg("nextTransition",nextTransition)
    dbg("inputBuffer",client.inputBuffer)
    dbg("micros",microseconds())
    
    if(nextTransition == False and len(client.inputBuffer) > 0):
      nextTransition = client.inputBuffer.popleft()[1]  
      
    if(nextTransition and microseconds() > nextTransition ):
      toggleSpeaker()
      nextTransition = False
      lastTransitionTime = microseconds()
       
    if((microseconds() - lastTransitionTime) > TIMEOUT_INTERVAL):
      setSpeaker(0)
      client.mode = "send"
      
    time.sleep(0.5)      
  '''

# GPIO.cleanup()
